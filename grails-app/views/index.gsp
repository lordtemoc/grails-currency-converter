<!DOCTYPE html>
<%@ page import="sec.treasurer.User"%>
<%@page import="org.codehaus.groovy.grails.commons.GrailsApplication"%>
<html>
	<g:javascript library="jquery"/>
	<head>
		<meta name="layout" content="main"/>
		<title>Grails Currency Converter</title>
		<style type="text/css" media="screen">
			#status {
				background-color: #eee;
				border: .2em solid #fff;
				margin: 2em 2em 1em;
				padding: 1em;
				width: 12em;
				float: left;
				-moz-box-shadow: 0px 0px 1.25em #ccc;
				-webkit-box-shadow: 0px 0px 1.25em #ccc;
				box-shadow: 0px 0px 1.25em #ccc;
				-moz-border-radius: 0.6em;
				-webkit-border-radius: 0.6em;
				border-radius: 0.6em;
			}

			.ie6 #status {
				display: inline; /* float double margin fix http://www.positioniseverything.net/explorer/doubled-margin.html */
			}

			#status ul {
				font-size: 0.9em;
				list-style-type: none;
				margin-bottom: 0.6em;
				padding: 0;
			}

			#status li {
				line-height: 1.3;
			}

			#status h1 {
				text-transform: uppercase;
				font-size: 1.1em;
				margin: 0 0 0.3em;
			}

			#page-body {
				margin: 2em 1em 1.25em 2em;
			}

			h2 {
				margin-top: 1em;
				margin-bottom: 0.3em;
				font-size: 1em;
			}

			p {
				line-height: 1.5;
				margin: 0.25em 0;
			}

			#controller-list ul {
				list-style-position: inside;
			}

			#controller-list li {
				line-height: 1.3;
				list-style-position: inside;
				margin: 0.25em 0;
			}
			
			#id{
			  margin-bottom: 5em;
			  background-color: red;
			}

			@media screen and (max-width: 480px) {
				#status {
					display: none;
				}

				#page-body {
					margin: 0 1em 1em;
				}

				#page-body h1 {
					margin-top: 0;
				}
			}
		</style>
		
	</head>
	<script type="text/javascript">function hola(){
		alert("HOLA");
		window.onload=hola();
	    }</script>
	<body>
		<a href="#page-body" class="skip"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
	    <script type="text/javascript">
	    var mx = 0;
	    var euro = 0;
	    var us = 0;
	    var can = 0;
	    var gbp = 0;
	    var yen = 0;

	    
	    
	    function isNumberKey(evt){
	    	var elem = document.getElementById("texto");
	    	var primerCaracter = elem.value.indexOf('.');
	    	if(primerCaracter!=-1 && (evt.keyCode==46))
	    		return false;
	        var charCode = (evt.which) ? evt.which : evt.keyCode
	        		if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57)))
	            return false;   	        
	        return true;
	    }

function cualMoneda(moneda){	    	
	    	switch(moneda.charAt(0)){
	    	case '1': return "Mexican Pesos"; break;	
	    	case '2': return "Euros"; break;
	    	case '3': return "US Dollars"; break;
	    	case '4': return "Canadian Dollars"; break;
	    	case '5': return "GBP"; break;
	    	case '6': return "Yen"; break;
	    	}
	    	return 500;
	    }
	    
	    function prueba(evt){
	    	var elem = document.getElementById("texto");    
	        var elem2 = document.getElementById("texto2");
	        elem2.value = elem.value*2;
	        return true;
	    }

	    function conversor(evt){		
	    	var valor1 = document.getElementById("texto");    
	    	var valor2 = document.getElementById("texto2");
	    	var moneda1 = document.getElementById("combo");    
	    	var moneda2 = document.getElementById("combo2");
	    	if(valor1.value != 0 && valor1.value != "")
		    	{
	    	valor2.value=valor1.value/cambiarMoneda(moneda1.value)*cambiarMoneda(moneda2.value);  
	    	var descripcion = valor1.value +" "+cualMoneda(moneda1.value) + " = " + valor2.value +" " + cualMoneda(moneda2.value);
	    	<g:remoteFunction controller="user" action="hola" params="\'descripcion=\'+descripcion"/>; 
		    	}
	        return true;
	    }

	    function conversor2(evt){		
	    	var valor1 = document.getElementById("texto");    
	    	var valor2 = document.getElementById("texto2");
	    	var moneda1 = document.getElementById("combo");    
	    	var moneda2 = document.getElementById("combo2");
	    	if(valor2.value != 0 && valor2.value != "")
	    	{
	    	valor1.value=valor2.value/cambiarMoneda(moneda2.value)*cambiarMoneda(moneda1.value); 
	    	var descripcion = valor2.value +" "+cualMoneda(moneda2.value) + " = " + valor1.value +" " + cualMoneda(moneda1.value);
	    	<g:remoteFunction controller="user" action="hola" params="\'descripcion=\'+descripcion"/>;   
	    	}
	        return true;
	    }
	    function cargaDatos(){
		    if(mx==0){
		    	<g:remoteFunction controller="user" action="regresaMx" onSuccess="mxPesos(data)"/>;
		    	<g:remoteFunction controller="user" action="regresaDollar" onSuccess="usDollar(data)"/>;
		    	<g:remoteFunction controller="user" action="regresaEuro" onSuccess="euroValor(data)"/>;
		    	<g:remoteFunction controller="user" action="regresaCan" onSuccess="canDollar(data)"/>;
		    	<g:remoteFunction controller="user" action="regresaGBP" onSuccess="gbpValor(data)"/>;
		    	<g:remoteFunction controller="user" action="regresaYen" onSuccess="yenValor(data)"/>;	    	
		    	<g:remoteFunction controller="user" action="registro2"/>;   
			    }	    				
	    }
	    function mxPesos(data){
			mx=data;
		    }
	    function usDollar(data){
			us=data;
		    }
	    function euroValor(data){
			euro=data;
		    }
	    function canDollar(data){
			can=data;
		    }
	    function gbpValor(data){
			gbp=data;
		    }
	    function yenValor(data){
			yen=data;
		    }

	    function cambiarMoneda(moneda){
	    	
	    	switch(moneda.charAt(0)){
	    	case '1': return mx; break;	
	    	case '2': return euro; break;
	    	case '3': return us; break;
	    	case '4': return can; break;
	    	case '5': return gbp; break;
	    	case '6': return yen; break;
	    	}
	    	return 500;
	    }

	    </script>
		<div id="page-body" role="main" onmouseover="cargaDatos()">
			<h1>Welcome to Grails Currency Converter</h1>
			<p>¡Pruebame!</p>	  
			  <br>
			   
			<SELECT id="combo" NAME="selCombo" SIZE=1 onChange="return conversor(event)">
				<OPTION VALUE=3>US Dollars</OPTION>
				<OPTION VALUE=1>Mexican Pesos</OPTION>
				<OPTION VALUE=4>Canadian Dollars</OPTION>
				<OPTION VALUE=2>EUROS</OPTION>
				<OPTION VALUE=5>GBP</OPTION>
				<OPTION VALUE=6>Yen</OPTION>
			</SELECT> 
			<input id="texto" type="text" name="cantidad" value=0 onkeypress="return isNumberKey(event)" onkeyup="return conversor(event)">
			<br/>
			<SELECT id="combo2" NAME="selCombo2" SIZE=1 onChange="return conversor(event)">
				<OPTION VALUE=1>Mexican Pesos</OPTION>
				<OPTION VALUE=3>US Dollars</OPTION>
				<OPTION VALUE=4>Canadian Dollars</OPTION>
				<OPTION VALUE=2>EUROS</OPTION>
				<OPTION VALUE=5>GBP</OPTION>
				<OPTION VALUE=6>Yen</OPTION>
			</SELECT> 
			<input id="texto2" type="text" name="cantidad2" value=0 onkeypress="return isNumberKey(event)" onkeyup="return conversor2(event)">
			<br/>
			
			<!--<input type="submit" id="submit" value="Submit" onclick="return hola()">  -->
			
			<br>
			<br>
			<br>
			<div id="controller-list" role="navigation">
			
			
				<h2>Configuracion avanzada:</h2>
				<br>
				<li><a class="admin" href="${createLink(uri:'/user/admin')}">Administración</a></li>
				
			</div>
		</div>
	</body>
</html>
