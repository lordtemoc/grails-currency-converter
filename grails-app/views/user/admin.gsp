<%@ page import="sec.treasurer.User"%>
<!doctype html>
<html>
<head>
<g:javascript library="jquery"/>
<meta name="layout" content="main">
<g:set var="entityName" value="ProjectTracker Admin"></g:set>
<title><g:message code="ProjectTracker Admin"
		args="[entityName]" /></title>
</head>
<body>
	<div id="page-body" role="main">
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri:'/')}">Principal</a></li>

			</ul>
		</div>
		<div id="controller-list" class="content scaffold-create" role="main">
			<h1>Administración</h1>
			<br>
			<script type="text/javascript">
				function registra(){
			    	<g:remoteFunction controller="user" action="registro"/>;   
					}
			</script>
			<div id="list" class="navigation">
				<ul>
				
					<li onclick="registra()"><a class="list" href="${createLink(uri:'/valores/')}">Administrar
							tipos de conversión</a></li>
					<br>
					<li><a class="list"
						href="${createLink(uri:'/transacciones/')}">Administrar
							transacciones</a></li>
					<br>
					<li><a class="list"
						href="${createLink(uri:'/transacciones/list/')}">Historial de
							transacciones</a></li>
					<br>
					<li><a class="list" href="${createLink(uri:'/valores/list/')}">Lista
							de valores monetarios</a></li>
					<br>
				</ul>
			</div>
			
		</div>
	</div>
</body>
</html>