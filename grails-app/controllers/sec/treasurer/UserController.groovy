package sec.treasurer

class UserController {
	
	

    def index() {
		redirect(action:"login")
	}
	
	def login={}
	def admin={}
	
	def authenticate={
		def user = User.findByUserNameAndPassword(params.userName, params.password)
		
		if(user){
			session.user=user
			flash.message="Hello ${user.fullName}!"
			redirect (action:"login")
		}
		else{
			flash.message="Sorry, ${params.userName}. Please try again."
			redirect(action:"login")
		}
	}
	
	def logout={
		flash.message="Goodbye ${session.user.fullName}"
		session.user=null
		redirect(action:"login")
	}
	
	def regresaDollar(){
		Valores v = Valores.get(3)
		def valor = (v.valor) as String
		render valor as String
	}
	
	def regresaMx(){
		Valores v = Valores.get(1)
		def valor = (v.valor) as String
		render valor as String
	}
	
	def regresaEuro(){
		Valores v = Valores.get(2)
		def valor = (v.valor) as String
		render valor as String
	}
	
	def regresaCan(){
		Valores v = Valores.get(4)
		def valor = (v.valor) as String
		render valor as String
	}
	
	def regresaGBP(){
		Valores v = Valores.get(5)
		def valor = (v.valor) as String
		render valor as String
	}
	
	def regresaYen(){
		Valores v = Valores.get(6)
		def valor = (v.valor) as String
		render valor as String
	}
	
	def registro(){
		Valores mx = Valores.get(1)
		Valores us = Valores.get(3)
		Valores ca = Valores.get(4)
		Valores eu = Valores.get(2)
		Valores gb = Valores.get(5)
		Valores ye = Valores.get(6)
		def valormx = (mx.valor) as String
		def valorus = (us.valor) as String
		def valorca = (ca.valor) as String
		def valoreu = (eu.valor) as String
		def valorgb = (gb.valor) as String
		def valorye = (ye.valor) as String
		
		def descripcion="Valores anteriores; Mexican Pesos: "+valormx+", US Dollars: "+valorus+", Canadian Dollars: "+valorca+", Euro: "+valoreu+", GBP: "+valorgb+", Yen: "+valorye
		
		def fecha = (new Date()) as String
		new Transacciones(descripcion:descripcion, fecha:fecha).save()		
	}
	
	def registro2(){
		Valores mx = Valores.get(1)
		Valores us = Valores.get(3)
		Valores ca = Valores.get(4)
		Valores eu = Valores.get(2)
		Valores gb = Valores.get(5)
		Valores ye = Valores.get(6)
		def valormx = (mx.valor) as String
		def valorus = (us.valor) as String
		def valorca = (ca.valor) as String
		def valoreu = (eu.valor) as String
		def valorgb = (gb.valor) as String
		def valorye = (ye.valor) as String		
		def descripcion="Valores actuales; Mexican Pesos: "+valormx+", US Dollars: "+valorus+", Canadian Dollars: "+valorca+", Euro: "+valoreu+", GBP: "+valorgb+", Yen: "+valorye		
		def fecha = (new Date()) as String
		new Transacciones(descripcion:descripcion, fecha:fecha).save()
	}
	
	def hola(String descripcion){
		def fecha = (new Date()) as String
		new Transacciones(descripcion:descripcion, fecha:fecha).save()		
	}
}
 	